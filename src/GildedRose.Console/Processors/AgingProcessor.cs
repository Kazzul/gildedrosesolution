﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GildedRose.Console.Processors
{
    public class AgingProcessor : ItemProcessor 
    {
        public AgingProcessor(Item item) : base(item)
        {}

        protected override void ChangeQuantity()
        {
            if (IsUnderMaxQuality())
            {
                if (IsExpired())
                {
                    item.Quality += 2;
                }
                else
                {
                    item.Quality += 1;
                }
            }
        }
    }
}
